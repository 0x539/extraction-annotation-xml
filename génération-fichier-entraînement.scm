#!/usr/bin/guile \
-e début -s
!#

;;
;; Auteur : Corentin Bocquillon <corentin@nybble.fr> 2018

;; Ce logiciel est régi par la licence CeCILL soumise au droit français et
;; respectant les principes de diffusion des logiciels libres. Vous pouvez
;; utiliser, modifier et/ou redistribuer ce programme sous les conditions
;; de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
;; sur le site « http://www.cecill.info ».

;; En contrepartie de l'accessibilité au code source et des droits de copie,
;; de modification et de redistribution accordés par cette licence, il n'est
;; offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
;; seule une responsabilité restreinte pèse sur l'auteur du programme,  le
;; titulaire des droits patrimoniaux et les concédants successifs.

;; A cet égard  l'attention de l'utilisateur est attirée sur les risques
;; associés au chargement,  à l'utilisation,  à la modification et/ou au
;; développement et à la reproduction du logiciel par l'utilisateur étant
;; donné sa spécificité de logiciel libre, qui peut le rendre complexe à
;; manipuler et qui le réserve donc à des développeurs et des professionnels
;; avertis possédant  des  connaissances  informatiques approfondies.  Les
;; utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
;; logiciel à leurs besoins dans des conditions permettant d'assurer la
;; sécurité de leurs systèmes et ou de leurs données et, plus généralement,
;; à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

;; Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
;; pris connaissance de la licence CeCILL, et que vous en avez accepté les
;; termes.

;; Génération d’un fichier d’entraînement.
;;

(add-to-load-path ".")

(use-modules (fonctions-utiles)
             (ice-9 threads)
             (ice-9 getopt-long))

;;; Niveaux les plus proches des mots.
;; (define symboles '(name demonym kind name.nickname extractor demonym.nickname
;;                         qualifier name.first name.last val unit day month year
;;                         range-mark name.middle title address-number po-box
;;                         zip-code other-address-component object century
;;                         millenium reference-era time-modifier award-cat))

;;; Niveaux les plus éloignés des mots.
;; (define symboles '(prod.media time.hour.abs pers.ind pers.coll loc.adm.reg
;;                               loc.adm.town amount func.ind func.coll org.ent
;;                               time.hour.rel org.adm loc.adm.nat
;;                               loc.adm.sup loc.phys.hydro loc.phys.geo
;;                               loc.phys.astro loc.oro loc.fac loc.add.phys
;;                               loc.add.elec prod.object prod.art prod.fin
;;                               prod.soft prod.award prod.doctr prod.rule
;;                               prod.other time.date.abs time.date.rel
;;                               event))

;;; Les événements
;; (define symboles '(event))

;;; Les organisations
;; (define symboles '(org.ent org.adm))

;;; Les montants
;; (define symboles '(amount))

;;; Les fonctions
;; (define symboles '(func.ind func.coll))

;;; Les personnes
;; (define symboles '(pers.ind pers.coll))

;;; Les produits
;; (define symboles '(prod.media prod.object prod.art prod.fin
;;                               prod.soft prod.award prod.doctr prod.rule
;;                               prod.other))

;;; Les lieux.
;; (define symboles '(loc.adm.reg loc.adm.town loc.adm.nat loc.adm.sup
;;                                loc.phys.hydro loc.phys.geo loc.phys.astro
;;                                loc.oro loc.fac loc.add.phys loc.add.elec))

;;; Les dates.
;; (define symboles '(time.hour.abs time.hour.rel time.date.abs time.date.rel))

(define (premier-élément-en-commun éléments liste)
  (cond ((null? éléments) #f)
        ((member (car éléments) liste) (car éléments))
        (else (premier-élément-en-commun (cdr éléments) liste))))

(define* (obtenir-colonne1 liste précédent liste-symboles
                           #:optional (accumulateur '()))
  (if (null? (cdr liste))
      (let ((élément (premier-élément-en-commun liste-symboles (cdar liste))))
        (append accumulateur
                (cond ((and élément (eq? précédent élément))
                       (list (string-append "I-" (symbol->string élément))))
                      (élément (list (string-append "B-"
                                                    (symbol->string élément))))
                      (else `("O")))))
      (let ((élément (premier-élément-en-commun liste-symboles (cdar liste))))
        (obtenir-colonne1
         (cdr liste) élément
         liste-symboles
         (append accumulateur
                 (cond ((and élément (eq? précédent élément))
                        (list (string-append "I-" (symbol->string élément))))
                       (élément
                        (list (string-append "B-" (symbol->string élément))))
                       (else `("O"))))))))

(define (générer-fichier liste-fichiers fichier-sortie liste-symboles)
  (for-each
   (lambda (fichier)
     (display (string-append "fichier : " fichier "\n"))
     (let* ((lignes (lire-lignes (open-input-file fichier)))
            (liste (obtenir-liste-mots-annotations lignes))
            (précédent (obtenir-colonne1 (list-head liste 1) #f liste-symboles))
            (colonne1 (obtenir-colonne1
                       (cdr liste)
                       (if (not (string=? "O" (car précédent)))
                           (string->symbol (car précédent)) #f)
                       liste-symboles)))

       (for-each
        (lambda (numéro)
          ;; Il faut ignorer les chaînes vides.
          (if (not (string=? (car (list-ref liste (1+ numéro))) ""))
              (begin
                (display (car (list-ref liste (1+ numéro))) fichier-sortie)
                (display "\t" fichier-sortie)
                (display (list-ref colonne1 numéro) fichier-sortie)
                (display "\n" fichier-sortie))))
        (iota (1- (length liste))))))
   liste-fichiers))

(define (début arguments)
  (let* ((spéc-option '((aide (single-char #\a) (value #f))
                        (help (single-char #\h) (value #f))
                        (préfixe (single-char #\p) (value #t) (required? #t))
                        (symboles (single-char #\s) (value #t)(required? #t))))
         (options (getopt-long arguments spéc-option))
         (aide-demandé (or (option-ref options 'aide #f)
                           (option-ref options 'help #f)))
         (liste-symboles (map string->symbol (string-split
                                              (option-ref options 'symboles #f)
                                              #\;))))
    (if (or aide-demandé (not options))
        (begin
          (write options)
          (newline)
          (display (string-append
                    "\n" (car arguments) " [options] chemin [chemin2 […]]
-a, --aide, -h, --help    Affiche cette aide.
-p, --préfixe             Définit le préfixe des fichiers créés
-s, --symboles SYMBOLES   Définit la liste des symboles. Ils doivent tous être
                          séparés par des points-virgules :
                          symbole1;symbole2;…;symboleN"))
          (newline)
          (exit EXIT_SUCCESS)))

    (let* ((sortie-apprentissage (open-output-file
                                  (string-append (option-ref options 'préfixe "résultat")
                                                 ".train")))
           (sortie-contrôle (open-output-file
                             (string-append (option-ref options 'préfixe "résultat")
                                            ".test")))
           (fichiers (sort (option-ref options '() #f)
                           (lambda (x y)
                             (< (string-hash x) (string-hash y)))))
           (frontière (round (* 9 (/ (length fichiers) 10)))))
      (générer-fichier (list-head fichiers frontière)
                       sortie-apprentissage liste-symboles)
      (générer-fichier (list-tail fichiers frontière)
                       sortie-contrôle liste-symboles))))
