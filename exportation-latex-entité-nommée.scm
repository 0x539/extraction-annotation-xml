;;
;; Auteur : Corentin Bocquillon <corentin@nybble.fr> 2018

;; Ce logiciel est régi par la licence CeCILL soumise au droit français et
;; respectant les principes de diffusion des logiciels libres. Vous pouvez
;; utiliser, modifier et/ou redistribuer ce programme sous les conditions
;; de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
;; sur le site « http://www.cecill.info ».

;; En contrepartie de l'accessibilité au code source et des droits de copie,
;; de modification et de redistribution accordés par cette licence, il n'est
;; offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
;; seule une responsabilité restreinte pèse sur l'auteur du programme,  le
;; titulaire des droits patrimoniaux et les concédants successifs.

;; A cet égard  l'attention de l'utilisateur est attirée sur les risques
;; associés au chargement,  à l'utilisation,  à la modification et/ou au
;; développement et à la reproduction du logiciel par l'utilisateur étant
;; donné sa spécificité de logiciel libre, qui peut le rendre complexe à
;; manipuler et qui le réserve donc à des développeurs et des professionnels
;; avertis possédant  des  connaissances  informatiques approfondies.  Les
;; utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
;; logiciel à leurs besoins dans des conditions permettant d'assurer la
;; sécurité de leurs systèmes et ou de leurs données et, plus généralement,
;; à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

;; Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
;; pris connaissance de la licence CeCILL, et que vous en avez accepté les
;; termes.

;; Exportation d’une entité en tableau LaTeX.
;;

(add-to-load-path ".")

(define-module (exportation-latex-entité-nommée)
  #:use-module (sxml simple)
  #:use-module (sxml fold)
  #:use-module (fonctions-utiles))

(define-public (exporter-entité-nommée entité)
  (set! entité (supprimer-chaînes-vides-arbre entité))
  (let* ((nombre-colonnes (colonnes-nécessaires entité))
        (début-chaîne
         (string-append "\\begin{center}\n  \\begin{tabulary}{0.75\\textwidth}{|"
                        (obtenir-ligne-contenant-nombre-colonnes nombre-colonnes)
                        "}\n    \\hline\n"))
        (en-tête (string-append "    Texte" (obtenir-en-tête-tableau (- nombre-colonnes 1))))
        (corps (obtenir-corps-tableau nombre-colonnes entité))
        (fin-chaîne "  \\end{tabulary}\n\\end{center}"))
    (string-append début-chaîne en-tête corps fin-chaîne)))

(define (colonnes-nécessaires document)
  (1+ (obtenir-nombre-niveaux document)))

(define* (obtenir-ligne-contenant-nombre-colonnes nombre-colonnes
                                                  #:optional (compteur 0))
  (cond ((< compteur (- nombre-colonnes 1))
         (string-append " c |" (obtenir-ligne-contenant-nombre-colonnes
                                nombre-colonnes (+ compteur 1))))
        ((< compteur nombre-colonnes)
         " c |")))

(define* (obtenir-en-tête-tableau nombre-colonnes #:optional (compteur 0))
  (cond ((and (< compteur (- nombre-colonnes 1))
              (string-append
               " & " (number->string compteur)
               (obtenir-en-tête-tableau nombre-colonnes (+ compteur 1)))))
        ((< compteur nombre-colonnes)
         (string-append " & " (number->string compteur) " \\\\\n    \\hline\n"))
        (else "")))

(define (obtenir-colonne-tableau numéro-ligne numéro-colonne entité)
  (let ((parent-précédent (false-if-exception
                           (list-ref (obtenir-parents-nième-mot
                                      numéro-ligne entité)
                                     numéro-colonne)))
        (parent (false-if-exception
                 (list-ref (obtenir-parents-nième-mot (+ numéro-ligne 1)
                                                      entité)
                           numéro-colonne)))
        (parent-suivant (false-if-exception
                         (list-ref (obtenir-parents-nième-mot
                                    (+ numéro-ligne 2) entité)
                          numéro-colonne))))

    (cond ((not parent) "")
          ((eq? parent parent-précédent parent-suivant)
           (string-append "I." (symbol->string parent)))
          ((and (not (eq? parent parent-précédent)) (eq? parent parent-suivant))
           (string-append "B." (symbol->string parent)))
          ((not (eq? parent parent-précédent))
           (symbol->string parent))
          ((not (eq? parent parent-suivant))
           (string-append "E." (symbol->string parent))))))

(define (obtenir-colonnes-tableau numéro-ligne nombre-colonnes entité)
  (map (lambda (colonne)
         (obtenir-colonne-tableau numéro-ligne (+ colonne 1) entité))
       (iota (- nombre-colonnes 1))))

(define (obtenir-ligne-tableau numéro-ligne nombre-colonnes
                               entité contenu-balise)
  (let* ((colonnes (obtenir-colonnes-tableau
                    numéro-ligne nombre-colonnes entité))
         (colonnes-présentés (list-tail
                              (map (lambda (colonne)
                                     (string-append " & " colonne))
                                   colonnes) 1)))
    (string-append "    "
     (list-ref contenu-balise numéro-ligne) " & " (list-ref colonnes 0)
     (apply string-append colonnes-présentés) " \\\\\n    \\hline\n")))

(define (obtenir-corps-tableau nombre-colonnes entité)
  (let ((contenu-balise (mettre-à-plat
                         (map (lambda (chaîne)
                                (string-split
                                 (string-trim-both
                                  chaîne char-whitespace?) #\ ))
                              (obtenir-contenu-balise entité)))))
    (apply string-append (map (lambda (ligne)
                                (obtenir-ligne-tableau
                                 ligne nombre-colonnes entité contenu-balise))
                              (iota (length contenu-balise))))))
