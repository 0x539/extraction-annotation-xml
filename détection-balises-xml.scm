;;
;; Auteur : Corentin Bocquillon <corentin@nybble.fr> 2018

;; Ce logiciel est régi par la licence CeCILL soumise au droit français et
;; respectant les principes de diffusion des logiciels libres. Vous pouvez
;; utiliser, modifier et/ou redistribuer ce programme sous les conditions
;; de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
;; sur le site « http://www.cecill.info ».

;; En contrepartie de l'accessibilité au code source et des droits de copie,
;; de modification et de redistribution accordés par cette licence, il n'est
;; offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
;; seule une responsabilité restreinte pèse sur l'auteur du programme,  le
;; titulaire des droits patrimoniaux et les concédants successifs.

;; A cet égard  l'attention de l'utilisateur est attirée sur les risques
;; associés au chargement,  à l'utilisation,  à la modification et/ou au
;; développement et à la reproduction du logiciel par l'utilisateur étant
;; donné sa spécificité de logiciel libre, qui peut le rendre complexe à
;; manipuler et qui le réserve donc à des développeurs et des professionnels
;; avertis possédant  des  connaissances  informatiques approfondies.  Les
;; utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
;; logiciel à leurs besoins dans des conditions permettant d'assurer la
;; sécurité de leurs systèmes et ou de leurs données et, plus généralement,
;; à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

;; Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
;; pris connaissance de la licence CeCILL, et que vous en avez accepté les
;; termes.

;; Détection de balises XML dans le corpus quaero.
;;

(add-to-load-path ".")

(define-module (détection-balises-xml)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 textual-ports)
  #:use-module (sxml simple)
  #:use-module (ice-9 optargs)
  #:use-module (fonctions-utiles))

(define motif-balise-ouvrante "<[a-z]+(\\.|\\-)?[a-z]+(\\.?|\\-)[a-z]*>")
(define motif-balise-fermante "</[a-z]+(\\.|\\-)?[a-z]+(\\.?|\\-)[a-z]*>")

(define* (trouver-fin-contenu-balisé
          chaîne #:optional
          (début (false-if-exception
                  (match:end (string-match motif-balise-ouvrante chaîne))))
          (nombre 1))
  (if (or (= nombre 0) (not début))
      début
      (let ((balise-ouvrante (string-match motif-balise-ouvrante chaîne
                                           début))
            (balise-fermante (string-match motif-balise-fermante chaîne
                                           début)))
        (cond ((not (or balise-ouvrante balise-fermante))
               #f)
              ((and balise-ouvrante (< (match:start balise-ouvrante)
                                       (match:start balise-fermante)))
               (trouver-fin-contenu-balisé
                chaîne (match:end balise-ouvrante) (+ nombre 1)))
              (else
               (trouver-fin-contenu-balisé
                chaîne (match:end balise-fermante) (- nombre 1)))))))

(define*-public (obtenir-bornes-balise chaîne #:optional (début 0))
  (let* ((correspondance-début (string-match motif-balise-ouvrante
                                            chaîne début))
        (début-contenu (false-if-exception (match:start correspondance-début)))
        (fin-contenu (trouver-fin-contenu-balisé
                      chaîne
                      (false-if-exception (match:end correspondance-début)))))
    `(,début-contenu . ,fin-contenu)))

(define*-public (obtenir-balises chaîne #:optional (début 0) (accumulateur '()))
  (let ((bornes (obtenir-bornes-balise chaîne début)))
    (if (not (and (car bornes) (cdr bornes)))
        accumulateur
        (obtenir-balises
         chaîne (cdr bornes)
         (append accumulateur
                 `(,(substring chaîne (car bornes) (cdr bornes))))))))

(define*-public (obtenir-toutes-les-bornes-des-balises chaîne #:optional (début 0)
                                                (accumulateur '()))
  (let ((bornes (obtenir-bornes-balise chaîne début)))
    (if (not (and (car bornes) (cdr bornes)))
        accumulateur
        (obtenir-toutes-les-bornes-des-balises
         chaîne (cdr bornes) (append accumulateur `(,bornes))))))
