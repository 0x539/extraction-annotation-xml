#!/usr/bin/guile \
-e début -s
!#

;;
;; Auteur : Corentin Bocquillon <corentin@nybble.fr> 2018

;; Ce logiciel est régi par la licence CeCILL soumise au droit français et
;; respectant les principes de diffusion des logiciels libres. Vous pouvez
;; utiliser, modifier et/ou redistribuer ce programme sous les conditions
;; de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
;; sur le site « http://www.cecill.info ».

;; En contrepartie de l'accessibilité au code source et des droits de copie,
;; de modification et de redistribution accordés par cette licence, il n'est
;; offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
;; seule une responsabilité restreinte pèse sur l'auteur du programme,  le
;; titulaire des droits patrimoniaux et les concédants successifs.

;; A cet égard  l'attention de l'utilisateur est attirée sur les risques
;; associés au chargement,  à l'utilisation,  à la modification et/ou au
;; développement et à la reproduction du logiciel par l'utilisateur étant
;; donné sa spécificité de logiciel libre, qui peut le rendre complexe à
;; manipuler et qui le réserve donc à des développeurs et des professionnels
;; avertis possédant  des  connaissances  informatiques approfondies.  Les
;; utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
;; logiciel à leurs besoins dans des conditions permettant d'assurer la
;; sécurité de leurs systèmes et ou de leurs données et, plus généralement,
;; à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

;; Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
;; pris connaissance de la licence CeCILL, et que vous en avez accepté les
;; termes.

;; Transformation des annotations des fichiers en paramètres en
;; tableaux latex.
;;

(add-to-load-path ".")

(use-modules (sxml simple)
             (sxml fold)
             (exportation-latex-entité-nommée)
             (détection-balises-xml)
             (ice-9 textual-ports)
             (ice-9 threads))

(define* (lire-lignes #:optional (port (current-input-port))
                      (contenu ""))
  (let ((ligne (get-line port)))
    (if (not (eof-object? ligne))
        (lire-lignes port (string-append contenu ligne))
        contenu)))

(define total-fichier 0)
(define fichiers-exportés 0)
(define mutex-fichier-exportés (make-mutex))
(define mutex-sortie (make-mutex))
(define mutex-test (make-mutex))

(define (début arguments)
  (set! total-fichier (1- (length arguments)))
  (display "0 %" (current-error-port))
  (par-for-each
   (lambda (fichier)
     (let* ((port-fichier (open-input-file fichier))
           (balises (obtenir-balises (lire-lignes port-fichier))))
       (lock-mutex mutex-test)
       (for-each
        (lambda (entité)
          (let ((texte (exporter-entité-nommée (xml->sxml entité))))
            (lock-mutex mutex-sortie)
            (display (string-append "\\begin{center}\n  " entité "\n\\end{center}\n\n"))
            (display texte) (newline) (newline)
            (unlock-mutex mutex-sortie)))
        balises)
       (unlock-mutex mutex-test)
       (close port-fichier))
     (lock-mutex mutex-fichier-exportés)
     (set! fichiers-exportés (+ fichiers-exportés 1))
     (display (make-string 80 #\ ) (current-error-port))
     (display "\r" (current-error-port))
     (display (number->string (exact->inexact
                               (* 100 (/ fichiers-exportés total-fichier))))
              (current-error-port))
     (display " %" (current-error-port))
     (unlock-mutex mutex-fichier-exportés))
   (list-tail arguments 1)))
