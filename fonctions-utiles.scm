;;
;; Auteur : Corentin Bocquillon <corentin@nybble.fr> 2018

;; Ce logiciel est régi par la licence CeCILL soumise au droit français et
;; respectant les principes de diffusion des logiciels libres. Vous pouvez
;; utiliser, modifier et/ou redistribuer ce programme sous les conditions
;; de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
;; sur le site « http://www.cecill.info ».

;; En contrepartie de l'accessibilité au code source et des droits de copie,
;; de modification et de redistribution accordés par cette licence, il n'est
;; offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
;; seule une responsabilité restreinte pèse sur l'auteur du programme,  le
;; titulaire des droits patrimoniaux et les concédants successifs.

;; A cet égard  l'attention de l'utilisateur est attirée sur les risques
;; associés au chargement,  à l'utilisation,  à la modification et/ou au
;; développement et à la reproduction du logiciel par l'utilisateur étant
;; donné sa spécificité de logiciel libre, qui peut le rendre complexe à
;; manipuler et qui le réserve donc à des développeurs et des professionnels
;; avertis possédant  des  connaissances  informatiques approfondies.  Les
;; utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
;; logiciel à leurs besoins dans des conditions permettant d'assurer la
;; sécurité de leurs systèmes et ou de leurs données et, plus généralement,
;; à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

;; Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
;; pris connaissance de la licence CeCILL, et que vous en avez accepté les
;; termes.

;; Des fonctions utiles.
;;

(define-module (fonctions-utiles)
  #:use-module (ice-9 optargs)
  #:use-module (sxml simple)
  #:use-module (détection-balises-xml)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 threads))

(define exclusion-xml (make-mutex))

(define (obtenir-liste-mots-annotations-arbre arbre)
  (let ((contenu-balise
         (mettre-à-plat
          (map (lambda (chaîne)
                 (string-split
                  (string-trim-both
                   chaîne char-whitespace?) #\ ))
               (obtenir-contenu-balise arbre)))))
    (map (lambda (nombre)
           `(,(list-ref contenu-balise nombre) . ,(list-tail
                                                   (obtenir-parents-nième-mot
                                                    (1+ nombre) arbre) 1)))
         (iota (length contenu-balise)))))

(define* (obtenir-liste-mots-annotations-dans-et-entre-balises
          chaîne bornes #:optional (accumulateur '()))
  (let ((arbre (catch #t
                 (lambda ()
                   (obtenir-arbre-conforme
                    (substring chaîne (caar bornes) (cdar bornes))))
                 (lambda (clef . arguments)
                   (display "Exception : ")
                   (write clef)
                   (write " ")
                   (write arguments)
                   (newline)
                   (display (substring chaîne (caar bornes) (cdar bornes)))
                   (newline)
                   "erreur"))))
    (cond ((and (string? arbre) (string=? arbre "erreur") (not (null? (cdr bornes))))
           (obtenir-liste-mots-annotations-dans-et-entre-balises
            chaîne (cdr bornes) accumulateur))
          ((or (not arbre) (and (string? arbre) (string=? arbre "erreur")))
           accumulateur)
          ((null? (cdr bornes))
           (append accumulateur
                   (obtenir-liste-mots-annotations-arbre arbre)))
          (else (obtenir-liste-mots-annotations-dans-et-entre-balises
                 chaîne (cdr bornes) (append
                                      accumulateur
                                      (obtenir-liste-mots-annotations-arbre arbre)
                                      (obtenir-liste-mots
                                       (substring chaîne (cdar bornes)
                                                  (caadr bornes)))))))))

(define (obtenir-liste-mots chaîne)
  (false-if-exception
   (map
    list
    (map (lambda (élément)
           (string-delete (lambda (caractère)
                            (char=? caractère #\return))
                          élément))
         (delete "" (string-split chaîne (char-set #\  #\newline)))))))

;; Permet d’obtenir une liste des mots de la chaîne et les
;; annotations associés à ces mots.
;;
;; Retourne une liste de paires de la forme ((mot1 type1 type2 ...)
;; (mot2 type1 type2 ...)).
(define-public (obtenir-liste-mots-annotations chaîne)
  (let ((bornes (obtenir-toutes-les-bornes-des-balises chaîne)))
    (let ((test (append
                 (map list (delete "" (string-split (substring chaîne 0 (caar bornes)) #\ )))
                 (obtenir-liste-mots-annotations-dans-et-entre-balises chaîne bornes)
                 (map list
                      (delete "" (string-split (substring chaîne (cdar (last-pair bornes))) #\ ))))))
      test)))

(define-public (mettre-à-plat arbre)
  (cond ((or (not (list? arbre)) (null? arbre)) '())
        ((list? (car arbre))
         (if (not (null? (cdr arbre)))
             (append (mettre-à-plat (car arbre)) (mettre-à-plat (cdr arbre)))
             (mettre-à-plat (car arbre))))
        ((and (not (null? (cdr arbre))) (list? (car arbre))))
        ((not (null? (cdr arbre)))
         (append `(,(car arbre)) (mettre-à-plat (cdr arbre))))
        (else arbre)))

(define-public (obtenir-contenu-balise arbre)
  (mettre-à-plat
   (let ((chaînes (extraire-chaînes arbre)))
     (if (string? chaînes)
         `(,chaînes)
         chaînes))))

(define-public (extraire-chaînes arbre)
  (cond ((null? arbre) '())
        ((and (not (list? arbre)) (string? arbre))
         (string-trim-both arbre char-whitespace?))
        ((and (null? (cdr arbre)) (string? (car arbre)))
         (string-trim-both (car arbre) char-whitespace?))
        ((and (list? arbre) (symbol? (car arbre)))
         (let ((liste (map extraire-chaînes (cdr arbre))))
           (if (and (eq? (length liste) 1)
                    (or (string? (car liste)) (list? liste)))
               (car liste)
               (delete "" liste))))
        (else '())))

(define*-public (obtenir-niveau niveau arbre #:optional (niveau-actuel 0))
  (cond ((or (and (not (= niveau 0)) (not (positive? niveau)))
             (not (list? arbre))) '())
        ((< niveau-actuel niveau)
         (apply append
                (map (lambda (élément)
                       (obtenir-niveau niveau élément (+ niveau-actuel 1)))
                     (cdr arbre))))
        ((= niveau niveau-actuel)
         arbre)))

(define*-public (obtenir-nombre-niveaux entité #:optional (niveau 0))
  (let ((niveau-actuel (obtenir-niveau (+ niveau 1) entité)))
    (if (not (null? niveau-actuel))
        (obtenir-nombre-niveaux entité (+ niveau 1))
        niveau)))

(define*-public (obtenir-niveaux arbre #:optional (compteur 0)
                                 (nombre-niveaux (obtenir-nombre-niveaux arbre))
                                 (accumulateur '()))
  (cond ((= compteur nombre-niveaux)
         (append accumulateur (obtenir-niveau compteur arbre)))
        ((< compteur nombre-niveaux)
         (obtenir-niveaux arbre (+ compteur 1) nombre-niveaux
                          (append
                           accumulateur (obtenir-niveau compteur arbre))))))

(define-public (feuille? arbre)
  (or (string? arbre)
      (and (list? arbre)
           (= (length arbre) 2)
           (symbol? (car arbre))
           (string? (cadr arbre)))))

(define*-public (obtenir-parents-nième-mot n arbre #:optional (compteur 0)
                                           (parents '()))
  (cond ((>= compteur n) parents)
        ((or (and (list? arbre) (null? arbre)) (feuille? arbre))
         compteur)
        ((and (list? arbre) (symbol? (car arbre)) (not (null? (cdr arbre))))
         (obtenir-parents-nième-mot n (cdr arbre) compteur
                                    (append parents
                                            `(,(car arbre)))))
        ((and
          (list? arbre)
          (list? (car arbre))
          (feuille? (car arbre)))
         (let* ((liste (string-split (string-trim-both
                                      (cadr (car arbre))
                                      char-whitespace?) #\ ))
                (résultat (obtenir-parents-nième-mot
                          n (car arbre) (+ compteur (length liste))
                          (append parents `(,(caar arbre))))))
           (if (list? résultat)
               résultat
               (obtenir-parents-nième-mot n (cdr arbre)
                                          résultat
                                          parents))))
        ((string? (car arbre))
         (let ((liste (string-split (string-trim-both
                                     (car arbre)
                                     char-whitespace?) #\ )))
           (obtenir-parents-nième-mot
            n (cdr arbre) (+ compteur (length liste)) parents)))
        ((and (list? arbre) (not (symbol? (car arbre))))
         (let ((résultat (obtenir-parents-nième-mot
                          n (car arbre) compteur parents)))
           (if (list? résultat)
               résultat
               (obtenir-parents-nième-mot n (cdr arbre) résultat parents))))
        (else #f)))

(define-public (obtenir-arbre-conforme chaîne)
  (lock-mutex exclusion-xml)
  (let ((résultat (supprimer-chaînes-vides-arbre (xml->sxml chaîne))))
    (unlock-mutex exclusion-xml)
    résultat))

(define*-public (supprimer-chaînes-vides-arbre entité #:optional
                                               (accumulateur '()))
  (cond ((null? entité) accumulateur)
        ((list? (car entité))
         (supprimer-chaînes-vides-arbre
          (cdr entité) `(,@accumulateur ,(supprimer-chaînes-vides-arbre
                                          (car entité)))))
        ((and (string? (car entité)) (or (string= (car entité) " ")
                                         (string= (car entité) "")))
         (supprimer-chaînes-vides-arbre (cdr entité) accumulateur))
        (else
         (supprimer-chaînes-vides-arbre
          (cdr entité) (append accumulateur `(,(car entité)))))))

(define*-public (lire-lignes #:optional (port (current-input-port))
                             (contenu ""))
  (let ((ligne (get-line port)))
    (if (eof-object? ligne)
        contenu
        (lire-lignes port (string-append contenu "\n" ligne)))))
