#!/bin/bash

# Génération les fichiers.
parallel ./génération-fichier-entraînement.scm -p {1} -s {2} ~/Documents/data_train_etape/unix/*.ne ::::+ préfixes symboles

# Corrections des erreurs éventuelles
# ./corriger-erreurs.py *.train *.test

# Séparation des propositions.
./séparer-propositions.py *.train *.test
